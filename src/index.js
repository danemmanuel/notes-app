import React from 'react';
import ReactDOM from "react-dom";
import './index.css';

const Root = () => (
    <div>...</div>
)

ReactDOM.render(<Root />, document.getElementById('root'));
